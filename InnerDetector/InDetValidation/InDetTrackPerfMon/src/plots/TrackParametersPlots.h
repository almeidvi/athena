/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef INDETTRACKPERFMON_PLOTS_TRACKPARAMETERSPLOTS_H
#define INDETTRACKPERFMON_PLOTS_TRACKPARAMETERSPLOTS_H

/**
 * @file    TrackParametersPlots.h
 * @author  Marco Aparo <marco.aparo@cern.ch> 
 **/

/// local includes
#include "../PlotMgr.h"


namespace IDTPM {

  class TrackParametersPlots : public PlotMgr {

  public:

    /// Constructor
    TrackParametersPlots(
        PlotMgr* pParent,
        const std::string& dirName,
        const std::string& anaTag,
        const std::string& trackType );

    /// Destructor
    virtual ~TrackParametersPlots() = default;

    /// Book the histograms
    void initializePlots(); // needed to override PlotBase
    StatusCode bookPlots();

    /// Dedicated fill method (for tracks and/or truth particles)
    template< typename PARTICLE >
    StatusCode fillPlots( const PARTICLE& particle, float weight );

    /// Print out final stats on histograms
    void finalizePlots();

  private:

    std::string m_trackType;

    TH1* m_pt;
    TH1* m_eta;
    /// TODO - include more plots

  }; // class TrackParametersPlots

} // namespace IDTPM

#endif // > ! INDETTRACKPERFMON_PLOTS_TRACKPARAMETERSPLOTS_H
