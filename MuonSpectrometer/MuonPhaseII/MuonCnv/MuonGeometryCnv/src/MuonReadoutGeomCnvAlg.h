/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONGEOMETRYCNV_MUONREADOUTGEOMCNVALG_H
#define MUONGEOMETRYCNV_MUONREADOUTGEOMCNVALG_H

#include "TrkSurfaces/Surface.h" // Work around cppcheck false positive
#include <AthenaBaseComps/AthReentrantAlgorithm.h>
#include <StoreGate/WriteCondHandleKey.h>
#include <StoreGate/ReadCondHandleKey.h>
#include <StoreGate/CondHandleKeyArray.h>

#include <MuonReadoutGeometry/MuonDetectorManager.h>
#include <MuonReadoutGeometryR4/MuonDetectorManager.h>
#include <MuonIdHelpers/IMuonIdHelperSvc.h>
#include <ActsGeometryInterfaces/ActsGeometryContext.h>

/** The MuonReadoutGeomCnvAlg converts the Run4 Readout geometry build from the GeoModelXML into the legacy MuonReadoutGeometry.
 *  The algorithm is meant to serve as an adapter allowing to dynamically exchange individual components in the Muon processing chain
 *  by their Run4 / Acts equivalents
 * 
*/

class MuonReadoutGeomCnvAlg : public AthReentrantAlgorithm {
    public:
        MuonReadoutGeomCnvAlg(const std::string& name, ISvcLocator* pSvcLocator);
        ~MuonReadoutGeomCnvAlg() = default;

        StatusCode execute(const EventContext& ctx) const override;
        StatusCode initialize() override;
        bool isReEntrant() const override { return false; }
    
    private:
        StatusCode buildStation(const ActsGeometryContext& gctx,
                                MuonGM::MuonDetectorManager& mgr,
                                const Identifier& stationId,
                                PVLink world) const;

        StatusCode buildMdt(const ActsGeometryContext& gctx,
                            MuonGM::MuonDetectorManager* mgr,
                            PVLink world) const;

        StatusCode buildRpc(const ActsGeometryContext& gctx,
                            MuonGM::MuonDetectorManager* mgr,
                            PVLink world) const;

        StatusCode buildSTGC(const ActsGeometryContext& gctx,
                             MuonGM::MuonDetectorManager* mgr,
                             PVLink world) const;

        StatusCode buildMM(const ActsGeometryContext& gctx,
                           MuonGM::MuonDetectorManager* mgr,
                           PVLink world) const;


        
        StatusCode dumpAndCompare(const ActsGeometryContext& gctx,
                                  const MuonGMR4::RpcReadoutElement& refEle,
                                  const MuonGM::RpcReadoutElement& testEle) const;

        StatusCode dumpAndCompare(const ActsGeometryContext& gctx,
                                  const MuonGMR4::MdtReadoutElement& refEle,
                                  const MuonGM::MdtReadoutElement& testEle) const;

        StatusCode dumpAndCompare(const ActsGeometryContext& gctx,
                                  const MuonGMR4::MmReadoutElement& refEle,
                                  const MuonGM::MMReadoutElement& testEle) const;
        

        ServiceHandle<Muon::IMuonIdHelperSvc> m_idHelperSvc{this, "MuonIdHelperSvc", "Muon::MuonIdHelperSvc/MuonIdHelperSvc"};

        SG::WriteCondHandleKey<MuonGM::MuonDetectorManager> m_writeKey{this, "WriteKey", "MuonDetectorManager"};
        
        SG::ReadCondHandleKeyArray<ActsTrk::DetectorAlignStore> m_alignStoreKeys{this, "AlignmentKeys", {}, "Alignment key"};
        
        Gaudi::Property<bool> m_checkGeo{this, "checkGeo", false, "Checks the positions of the sensors"};
        const MuonGMR4::MuonDetectorManager* m_detMgr{nullptr};


 
};

#endif
