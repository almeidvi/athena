/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef COLLECTIONBASE_ICOLLECTIONDESCRIPTION_H
#define COLLECTIONBASE_ICOLLECTIONDESCRIPTION_H

#include <string>
#include <vector>
#include <typeinfo>


namespace pool {

  class ICollectionColumn;
  class ICollectionIndex;
  
  /** 
   * @class ICollectionDescription ICollectionDescription.h CollectionBase/ICollectionDescription.h
   *
   * An interface used to define the properties of a collection to be constructed and to retrieve 
   * these properties after construction. The schema editor of the collection should be used for 
   * any modifications to these properties after construction.
   */
  class ICollectionDescription
  {
  public:
    /// Returns the name of the collection
    virtual const std::string& name() const = 0;

    /// Returns the storage technology type of the collection.
    virtual const std::string& type() const = 0;

    /// Returns the connection to the database containing the collection.
    virtual const std::string& connection() const = 0;

    /** 
     * Returns the name reserved for the event reference Token column. If the name has not
     * been set by the user a default name is returned.
     */
    virtual const std::string& eventReferenceColumnName() const = 0;

    /**
     * Indicates whether the collection contains the event reference column
     */
    virtual bool hasEventReferenceColumn() const = 0;

    /**
     * Returns the number of columns (including the event reference column if it is used) in 
     * the collection.
     */
    virtual int numberOfColumns() const = 0;

    /**
     * Returns a description object for a column of the collection, given the name of
     * the column.
     *
     * @param columnName Name of column.
     */
    virtual const ICollectionColumn& column( const std::string& columnName ) const = 0;

    /// return pointer to Column or NULL if it's not found (will not throw exceptions)
    virtual const ICollectionColumn* columnPtr( const std::string& columnName ) const = 0;
    
    /**
     * Returns the number of Token columns (including the event reference column if it is used) 
     */
    virtual int numberOfTokenColumns() const = 0;

    /**
     * Returns a description object for a Token column of the collection, given the name of 
     * the column.
     *
     * @param columnName Name of column.
     */
    virtual const ICollectionColumn& tokenColumn( const std::string& columnName ) const = 0; 

    /**
     * Returns a description object for a Token column of the collection, given the position
     * of the column
     *
     * @param columnId Position of column.
     */
    virtual const ICollectionColumn& tokenColumn( int columnId ) const = 0; 

    /** 
     * Returns the number of Attribute columns 
     */
    virtual int numberOfAttributeColumns( ) const = 0;

    /**
     * Returns a description object for an Attribute column of the collection, given the name of 
     * the column.
     *
     * @param columnName Name of column.
     */
    virtual const ICollectionColumn& attributeColumn( const std::string& columnName ) const = 0; 

    /**
     * Returns a description object for an Attribute column of the collection, given the position
     * of the column.
     *
     * @param columnId Position of column in associated collection fragment.
     */
    virtual const ICollectionColumn& attributeColumn( int columnId ) const = 0; 

    /**
     * Returns the number of indices used by the collection.
     */
    virtual int numberOfIndices() const = 0;

    /**
     * Returns a description object for an index of the collection, given the name of the column on which
     * the index is applied.
     *
     * @param columnName Name of column on which index is applied.
     */
    virtual const ICollectionIndex& index( const std::string& columnName ) const = 0;

    /**
     * Returns a description object for an index of the collection, given the names of the columns on 
     * which the index is applied.
     *
     * @param columnNames Names of columns on which index is applied.
     */
    virtual const ICollectionIndex& index( const std::vector<std::string>& columnNames ) const = 0;

    /**
     * Returns a description object for an index of the collection, given the ID number of 
     * the index.
     *
     * @param indexId ID of index.
     */
    virtual const ICollectionIndex& index( int indexId ) const = 0;

    /**
     * Check if both Descriptions have the same columns
     *
     * @param rhs Collection description object to compare.
     */
    virtual bool equals( const ICollectionDescription& rhs ) const = 0;

    /**
     * Check if all columns from this Description are present in the rhs Description
     * and if they have the same type
     *
     * @param rhs Collection description object to compare.
     */
    virtual bool isSubsetOf( const ICollectionDescription& rhs ) const = 0;

    /// print out the description (optional debugging)
    virtual void printOut() const {}

  protected:
    /// Empty destructor.
    virtual ~ICollectionDescription() {}
  };

}

#endif

